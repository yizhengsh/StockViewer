Welcome to the StockViewer wiki!

Environment: Python3.3 + PyQt5 IDE: PyCharm 3.0

Usage: Just input the symbols of the stocks, press the button "Show" Then the program will get the data of stocks and refresh every 2 seconds.

Notes: 1. Support equities and mutual funds listed in Shanghai Security Exchange and Shenzhen Security Exchange